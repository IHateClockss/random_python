#! /bin/env python
from turtle import *
from random import randint
from sys import argv

def main():
    clrs = ["red","blue","green","yellow","turquoise"]

    if argv[1] == "--speed" or argv[1] == "-s":
        print(f"the winner is the {clrs[randint(0, 4)]} turtle")
        return
    
    speed(0)
    tracer(0)
    penup()
    goto(-300,100)
    hideturtle()

    for step in range(30):
        write(step,align="center")
        right(90)
        for _ in range(10):
            penup()
            fd(10)
            pd()
            fd(10)
        pu()
        bk(200)
        lt(90)
        fd(20)

    tracer(1)
    turtles = []
    num = 80

    for clr in clrs:
        turt = Turtle()
        turt.color(clr)
        turt.shape("turtle")
        turt.pu()
        turt.goto(-300,num)
        num -= 40
        turt.pd()
        for _ in range(10):
            turt.right(36)
        turtles.append(turt)

    for _ in range(100):
        for turt in turtles:
            turt.fd(randint(1,10))
    good = 0

    for idx, turt in enumerate(turtles):
        if turtles[good].xcor() < turt.xcor():
            good = idx

    turt = Turtle()
    turt.hideturtle()
    turt.pu()
    turt.goto(0, -300)
    turt.write(f"the winner is {clrs[good]} turtle",align="center")
    print(f"the winner is the {clrs[good]} turtle")

    mainloop()

if __name__ == "__main__":
    main()
