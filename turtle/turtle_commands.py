#! /bin/env python
from turtle import *

def main():
    while True:
        commands = input("> ").split()
        st()
        pd()
        if commands.__len__() != 0:
            match commands[0]:
                case "square":
                    if commands.__len__() != 2:
                        print(f"command square only takes 1 argument but {commands.__len__()} were provided")
                        break
                    for _ in range(4):
                        fd(float(commands[1]))
                        rt(90)
                case "circ":
                    if commands.__len__() != 2:
                        print(f"command circ only takes 1 argument but {commands.__len__()} were provided")
                    circle(float(commands[1]))
                case "rect":
                    if commands.__len__() != 3:
                        print(f"command rect only takes 2 argument but {commands.__len__()} were provided")
                        break
                    for _ in range(2):
                        fd(float(commands[1]))
                        rt(90)
                        fd(float(commands[2]))
                        rt(90)
                case _:
                    print(f"unknown command: \"{" ".join(commands)}\"")
        
        ht()
        pu()
        home()

if __name__ == "__main__":
    main()
