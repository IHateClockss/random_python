#! /bin/env python
from turtle import *

def main():
    pd()

    fd(150)
    right(120)
    fd(150)
    right(120)
    fd(150)
    right(120)

    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
