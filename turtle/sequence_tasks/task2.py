#! /bin/env python
from turtle import *

def main():
    fillcolor("blue")
    begin_fill()
    pencolor("purple")

    fd(180)
    right(90)
    fd(180)
    right(90)
    fd(180)
    right(90)
    fd(180)

    end_fill()
    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
