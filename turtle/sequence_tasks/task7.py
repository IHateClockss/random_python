#! /bin/env python
from turtle import *

def main():
    speed(0)
    setup(800, 500)
    bgcolor("royalblue")
    pu()

    goto(-400, 250)
    goto(-400, 250)

    pd()
    pensize(65)
    color("white")

    rt(31.85)
    fd(920)

    pu()
    goto(-410, -250)
    pd()

    rt(296.3)
    fd(920)

    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
