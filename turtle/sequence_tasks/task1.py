#! /bin/env python
from turtle import *

def main():
    fillcolor("Brown")
    pencolor("Red")
    begin_fill()

    fd(100)
    right(90)
    fd(100)
    right(90)
    fd(100)
    right(90)
    fd(100)

    end_fill()
    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
