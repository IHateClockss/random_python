#! /bin/env python
from turtle import *

def main():
    clrs = ["black", "yellow", "red"]
    x_start = -300

    for clr in clrs:
        pu()
        goto(x_start, 200)
        pencolor(clr)
        fillcolor(clr)
        begin_fill()
        pd()

        fd(200)
        rt(90)
        fd(400)
        rt(90)
        fd(200)
        rt(90)
        fd(400)
        rt(90)

        end_fill()
        x_start += 200

    ht()
    mainloop()

if __name__ == "__main__":
    main()
