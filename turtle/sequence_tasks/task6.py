#! /bin/env python
from turtle import *

def main():
    pd()

    fillcolor("blue")
    pencolor("blue")
    begin_fill()
    for _ in range(10):
        fd(100)
        right(36)
    end_fill()

    fillcolor("red")
    pencolor("red")
    begin_fill()
    for _ in range(8):
        fd(100)
        right(45)
    end_fill()

    fillcolor("orange")
    pencolor("orange")
    begin_fill()
    for _ in range(6):
        fd(100)
        right(60)
    end_fill()

    fillcolor("purple")
    pencolor("purple")
    begin_fill()
    for _ in range(5):
        fd(100)
        right(72)
    end_fill()

    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
