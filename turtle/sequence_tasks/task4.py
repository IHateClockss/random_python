#! /bin/env python
from turtle import *

def main():
    pd()

    fd(150)
    right(90)
    fd(80)
    right(90)
    fd(150)
    right(90)
    fd(80)

    pu()
    fd(10)
    pd()

    fillcolor("red")
    pencolor("orange")
    begin_fill()

    fd(120)
    left(90)
    fd(190)
    left(90)
    fd(120)
    left(90)
    fd(190)

    end_fill()
    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
