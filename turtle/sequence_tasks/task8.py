#! /bin/env python
from turtle import *

def main():
    clrs = ["black", "red", "gold"]
    y_start = 150

    for clr in clrs:
        pu()
        goto(-200, y_start)
        color(clr)
        fillcolor(clr)
        begin_fill()
        pd()

        fd(400)
        rt(90)
        fd(100)
        rt(90)
        fd(400)
        rt(90)
        fd(100)
        rt(90)

        end_fill()
        y_start -= 100

    ht()
    mainloop()

if __name__ == "__main__":
    main()
