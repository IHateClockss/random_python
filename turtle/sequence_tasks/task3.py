#! /bin/env python
from turtle import *

def main():
    pd()

    fd(150)
    right(90)
    fd(80)
    right(90)
    fd(150)
    right(90)
    fd(80)

    pu()
    ht()
    mainloop()

if __name__ == "__main__":
    main()
